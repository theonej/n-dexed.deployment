﻿using System;
using System.Collections.Generic;

using N_Dexed.Deployment.Common.Domain.Commands;
using N_Dexed.Deployment.AWS.Commands;

namespace N_Dexed.Deployment.Configuration
{
    public static class Bootstrapper
    {
        public static DependencyController CreateDependencyController()
        {
            DependencyController controller = new DependencyController();

            controller.RegisterTypes();

            return controller;
        }

        /// <summary>
        /// Register your concrete implementations here
        /// </summary>
        /// <param name="controller"></param>
        private static void RegisterTypes(this DependencyController controller)
        {
            controller.Register<ICommandProcessor, AwsCommandProcessor>();
        }
    }
}
