﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace N_Dexed.Deployment.Common.Domain.Customer
{
    public interface ICredentials
    {
        string AccessKey { get; set; }
        string SecretKey { get; set; }
    }
}
