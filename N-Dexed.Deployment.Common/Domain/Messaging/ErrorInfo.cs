﻿namespace N_Dexed.Deployment.Common.Domain.Messaging
{
    public class ErrorInfo
    {
        string ErrorCode { get; set; }
        string Message { get; set; }
        string StackTrace { get; set; }
        object Data { get; set; }
        ErrorInfo InnerError { get; set; }

        /// <summary>
        /// Unwraps all inner exceptions and returns the last one
        /// </summary>
        /// <returns></returns>
        public ErrorInfo GetInnermostError()
        {
            ErrorInfo returnValue = this;

            if (this.InnerError != null)
            {
                returnValue = this.InnerError.GetInnermostError();
            }

            return returnValue;
        }
    }
}
