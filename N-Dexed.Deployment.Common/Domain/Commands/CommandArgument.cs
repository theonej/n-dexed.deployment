﻿using System;

namespace N_Dexed.Deployment.Common.Domain
{
    public class CommandArgument : IItemInfo
    {
        public Guid Id { get; set; }
        string Name { get; set; }
        object Value { get; set; }
        int Ordinal { get; set; }
    }
}
