﻿using System;
using System.Collections.Generic;
using System.Configuration;

using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;

using Newtonsoft.Json;

using N_Dexed.Deployment.Common.Domain;
using N_Dexed.Deployment.Common.Domain.Repositories;
using N_Dexed.Deployment.Common.Resources;

namespace N_Dexed.Deployment.AWS.Repositories
{
    public class DynamoCommandLibraryRepository : IRepository<CommandLibraryInfo>
    {
        private const string COMMAND_LIBRARY_TABLE_NAME = "n-dexed.deployment.commandLibraries";
        private const string COMMAND_LIBRARY_ID_COLUMN = "CommandLibraryId";
        private const string CUSTOMER_ID_COLUMN = "CustomerId";
        private const string SERIALIZED_DATA_COLUMN = "SerlializedColumn";

        public CommandLibraryInfo Get(CommandLibraryInfo item)
        {
            AmazonDynamoDBClient client = InitializeClient();
            using (client)
            {
                GetItemRequest request = CreateGetItemRequest(item);

                GetItemResponse response = client.GetItem(request);

                if (response.Item == null )
                {
                    string errorMessage = string.Format(ErrorMessages.MisingResponseItem, "Get Command Library");
                    throw new MissingFieldException(errorMessage);
                }

                CommandLibraryInfo returnValue = GetItemFromAttributeStore(response.Item);

                return returnValue;
            }
        }

        public Guid Save(CommandLibraryInfo item)
        {
            AmazonDynamoDBClient client = InitializeClient();
            using (client)
            {
                PutItemRequest request = CreatePutItemRequest(item);

                client.PutItem(request);

                return item.Id;
            }
        }

        public List<CommandLibraryInfo> Search(CommandLibraryInfo searchCriteria)
        {
           AmazonDynamoDBClient client = InitializeClient();
           using (client)
           {
               ScanRequest request = CreateScanRequest(searchCriteria);

               ScanResponse response = client.Scan(request);

               if (response.Items == null)
               {
                   string errorMessage = string.Format(ErrorMessages.MisingResponseItem, "Search Command Library");
                   throw new MissingFieldException(errorMessage);
               }

               List<CommandLibraryInfo> returnValue = new List<CommandLibraryInfo>();
               foreach (Dictionary<string, AttributeValue> item in response.Items)
               {
                   CommandLibraryInfo commandLibrary = GetItemFromAttributeStore(item);
                   returnValue.Add(commandLibrary);
               }

               return returnValue;
           }
        }

        public void Delete(CommandLibraryInfo item)
        {
            AmazonDynamoDBClient client = InitializeClient();
            using (client)
            {
                DeleteItemRequest request = CreateDeleteItemRequest(item);

                client.DeleteItem(request);
            }
        }

        #region Private Methods

        private AmazonDynamoDBClient InitializeClient()
        {
            AmazonDynamoDBConfig config = new AmazonDynamoDBConfig();
            config.ServiceURL = ConfigurationManager.AppSettings["ServiceURL"]; 

            AmazonDynamoDBClient client = new AmazonDynamoDBClient(config);
            
            //other initialization actions here

            return client;
        }

        private PutItemRequest CreatePutItemRequest(CommandLibraryInfo commandLibrary)
        {
            PutItemRequest request = new PutItemRequest();

            request.TableName = COMMAND_LIBRARY_TABLE_NAME;

            request.Item = new Dictionary<string, AttributeValue>();
            
            request.Item.Add(COMMAND_LIBRARY_ID_COLUMN, GetItemAttributeStringValue(commandLibrary.Id));
            request.Item.Add(CUSTOMER_ID_COLUMN, GetItemAttributeStringValue(commandLibrary.CustomerId));
            request.Item.Add(SERIALIZED_DATA_COLUMN, GetItemAttributeSerializedValue(commandLibrary));

            return request;
        }

        private ScanRequest CreateScanRequest(CommandLibraryInfo searchCriteria)
        {
            ScanRequest request = new ScanRequest();

            request.TableName = COMMAND_LIBRARY_TABLE_NAME;
            request.ScanFilter = new Dictionary<string, Condition>
            {
                {
                    CUSTOMER_ID_COLUMN,
                    new Condition() 
                    {
                        ComparisonOperator = Constants.DYNAMO_EQUALITY_OPERATOR, 
                        AttributeValueList = new List<AttributeValue>()
                        {
                            GetItemAttributeStringValue(searchCriteria.CustomerId)
                        }
                    }
               }
            };

            return request;
        }
        private CommandLibraryInfo GetItemFromAttributeStore(Dictionary<string, AttributeValue> attributeStore)
        {
            if(! attributeStore.ContainsKey(SERIALIZED_DATA_COLUMN))
            {
                string errorMessage = string.Format(ErrorMessages.MissingRequiredAttribute, SERIALIZED_DATA_COLUMN);
                throw new MissingFieldException(errorMessage);
            }
            string serializedValue = attributeStore[SERIALIZED_DATA_COLUMN].S;
            if(string.IsNullOrEmpty(serializedValue))
            {
                string errorMessage = string.Format(ErrorMessages.MissingRequiredAttribute, SERIALIZED_DATA_COLUMN);
                throw new DataMisalignedException(errorMessage);
            }

            CommandLibraryInfo returnValue = JsonConvert.DeserializeObject<CommandLibraryInfo>(serializedValue);

            return returnValue;
        }

        private DeleteItemRequest CreateDeleteItemRequest(CommandLibraryInfo item)
        {
            DeleteItemRequest request = new DeleteItemRequest();

            request.TableName = COMMAND_LIBRARY_TABLE_NAME;
            request.Key = new Dictionary<string, AttributeValue>()
            { 
              {COMMAND_LIBRARY_ID_COLUMN, GetItemAttributeStringValue(item.Id) },
              {CUSTOMER_ID_COLUMN, GetItemAttributeStringValue(item.CustomerId)}
            };

            return request;
        }

        private GetItemRequest CreateGetItemRequest(CommandLibraryInfo item)
        {
            GetItemRequest request = new GetItemRequest();

            request.TableName = COMMAND_LIBRARY_TABLE_NAME;

            request.Key = new Dictionary<string, AttributeValue>()
            { 
              {COMMAND_LIBRARY_ID_COLUMN, GetItemAttributeStringValue(item.Id) },
              {CUSTOMER_ID_COLUMN, GetItemAttributeStringValue(item.CustomerId)}
            };

            return request;
        }

        private AttributeValue GetItemAttributeStringValue(object item)
        {
            AttributeValue value = new AttributeValue();

            if(item != null)
                value.S = item.ToString();

            return value;
        }

        private AttributeValue GetItemAttributeSerializedValue(object item)
        {
            AttributeValue value = new AttributeValue();

            if (item != null)
                value.S = JsonConvert.SerializeObject(item);

            return value;
        }

        #endregion
    }
}
